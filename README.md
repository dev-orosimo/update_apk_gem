# UpdateApkGem
Gem for auto update the apk of your app.

## Usage
You will need to create a folder named 'apk' inside the 'app' folder of your project where you would add later your latest apk with the name 'update_<APKCODEVERSION>_.apk'. Also you will need to add on routes.rb the following command.
```ruby
mount UpdateApkGem::Engine => "/api"
```

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'update_apk_gem'
```
or
```ruby
gem "update_apk_gem", git: "https://gitlab.com/dev-orosimo/update_apk_gem.git", branch: 'main'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install update_apk_gem
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
